#import <Foundation/Foundation.h>
#import <Virtualization/Virtualization.h>
#include <stdio.h>
#include <termios.h>

@interface VMDelegate : NSObject <VZVirtualMachineDelegate>
@end

@implementation VMDelegate
- (void)guestDidStopVirtualMachine:(VZVirtualMachine *)virtualMachine {
  NSLog(@"VM stopped");
  exit(0);
}

- (void)virtualMachine:(VZVirtualMachine *)virtualMachine
      didStopWithError:(NSError *)error {
  NSLog(@"VM stopped with error: %@", [error localizedDescription]);
  exit(1);
}
@end

static struct termios orig_termios;
static void restore_term(void) {
  tcsetattr(STDIN_FILENO, TCSANOW, &orig_termios);
  printf("\r");
}

static int setterm() {
  if (!isatty(STDIN_FILENO))
    return 0;

  if (tcgetattr(STDIN_FILENO, &orig_termios)) {
    perror("tcgetattr stdin");
    return 1;
  }
  atexit(restore_term);

  struct termios termios = orig_termios;
  cfmakeraw(&termios);
  termios.c_cflag |= CLOCAL;

  if (tcsetattr(STDIN_FILENO, TCSANOW, &termios)) {
    perror("tcsetattr stdin");
    return 1;
  }

  return 0;
}

static void setBootLoader(VZVirtualMachineConfiguration *conf,
                          const char *kernel, const char *initrd,
                          const char *cmdline) {
  @autoreleasepool {
    NSURL *kernelURL =
        [NSURL fileURLWithPath:[[NSString alloc] initWithUTF8String:kernel]];
    VZLinuxBootLoader *bl =
        [[VZLinuxBootLoader alloc] initWithKernelURL:kernelURL];

    if (initrd) {
      NSURL *initrdURL =
          [NSURL fileURLWithPath:[[NSString alloc] initWithUTF8String:initrd]];
      [bl setInitialRamdiskURL:initrdURL];
    }

    if (cmdline)
      [bl setCommandLine:[[NSString alloc] initWithUTF8String:cmdline]];

    [conf setBootLoader:bl];
  }
}

static void setConsole(VZVirtualMachineConfiguration *conf) {
  @autoreleasepool {
    VZVirtioConsoleDeviceSerialPortConfiguration *cc =
        [[VZVirtioConsoleDeviceSerialPortConfiguration alloc] init];
    NSFileHandle *fhStdin = [NSFileHandle fileHandleWithStandardInput];
    NSFileHandle *fhStdout = [NSFileHandle fileHandleWithStandardOutput];
    [cc setAttachment:[[VZFileHandleSerialPortAttachment alloc]
                          initWithFileHandleForReading:fhStdin
                                  fileHandleForWriting:fhStdout]];
    [conf setSerialPorts:@[ cc ]];
  }
}

void addDisk(NSMutableArray *disks, const char *path, BOOL readonly) {
  @autoreleasepool {
    NSError *err = nil;
    NSURL *image =
        [NSURL fileURLWithPath:[[NSString alloc] initWithUTF8String:path]];

    VZDiskImageStorageDeviceAttachment *attachment =
        [[VZDiskImageStorageDeviceAttachment alloc] initWithURL:image
                                                       readOnly:readonly
                                                          error:&err];
    if (err) {
      NSLog(@"attach disk %s: %@", path, [err localizedDescription]);
      exit(1);
    }

    [disks addObject:[[VZVirtioBlockDeviceConfiguration alloc]
                         initWithAttachment:attachment]];
  }
}

static void setNetwork(VZVirtualMachineConfiguration *conf, const char *mac) {
  @autoreleasepool {
    VZVirtioNetworkDeviceConfiguration *nc =
        [[VZVirtioNetworkDeviceConfiguration alloc] init];
    nc.MACAddress = [[VZMACAddress alloc]
        initWithString:[[NSString alloc] initWithUTF8String:mac]];
    nc.attachment = [[VZNATNetworkDeviceAttachment alloc] init];
    conf.networkDevices = @[ nc ];
  }
}

int main(int argc, char **argv) {
  char *kernel = 0, *initrd = 0, *cmdline = 0, *mac = 0;
  int ch, cpus = 0, mem = 0;
  NSMutableArray *disks = [[NSMutableArray alloc] init];
  NSError *err = nil;

  while ((ch = getopt(argc, argv, "k:i:a:C:M:c:d:U:")) != -1) {
    switch (ch) {
    case 'k':
      kernel = optarg;
      break;
    case 'i':
      initrd = optarg;
      break;
    case 'a':
      cmdline = optarg;
      break;
    case 'C':
      cpus = atoi(optarg);
      break;
    case 'M':
      mem = atoi(optarg);
      break;
    case 'c':
      addDisk(disks, optarg, true);
      break;
    case 'd':
      addDisk(disks, optarg, false);
      break;
    case 'U':
      mac = optarg;
      break;
    }
  }

  if (!kernel) {
    printf("missing -k (uncompressed kernel) argument\n");
    return 1;
  }

  VZVirtualMachineConfiguration *conf =
      [[VZVirtualMachineConfiguration alloc] init];
  setBootLoader(conf, kernel, initrd, cmdline);
  setConsole(conf);
  if (mac)
    setNetwork(conf, mac);
  [conf
      setEntropyDevices:@[ [[VZVirtioEntropyDeviceConfiguration alloc] init] ]];
  [conf setStorageDevices:disks];
  [conf setCPUCount:cpus];
  [conf setMemorySize:mem * (1LL << 20)];

  if (![conf validateWithError:&err]) {
    NSLog(@"validate config: %@", [err localizedDescription]);
    return 1;
  }

  if (setterm())
    return 1;

  VZVirtualMachine *vm = [[VZVirtualMachine alloc] initWithConfiguration:conf];
  vm.delegate = [[VMDelegate alloc] init];
  [vm startWithCompletionHandler:^(NSError *err) {
    if (err) {
      NSLog(@"start VM: %@", [err localizedDescription]);
      exit(1);
    }
  }];

  dispatch_main();
}
