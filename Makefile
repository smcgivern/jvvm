LDFLAGS = -framework Foundation -framework Virtualization
EXE = jvvm
PREFIX = ~

all: $(EXE) sign

install: all
	install $(EXE) jvxh $(PREFIX)/bin/

format:
	clang-format -i *.m

sign:
	codesign --entitlements entitlements.plist --force -s - $(EXE)

clean:
	rm -f $(EXE)
